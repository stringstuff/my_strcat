/* 
 * my_strcat - concatenate two strings to a new string
 * also incorporating my_strcpy (string copy)
 */

#include <stdio.h>
#include <stdlib.h>

int my_strlen(char *);
char * my_strcat(char *, char *);
char * my_strcpy(char *, char *);

int main(int argc, char **argv) {
  if (argc < 3) {
    printf("Usage: %s <str1> <str2>\n", argv[0]);
    return 0;
  }

  char *str = NULL;
	  
  str = malloc((my_strlen(argv[1]) + my_strlen(argv[2])) * sizeof(char) + 1);

  my_strcpy(str, my_strcat(argv[1], argv[2]));

  printf("%s\n", str);

  free(str);

  return 0;
}

int my_strlen(char *s) {
  char *tmp = s;
  while (*s != '\0') {
    s++;
  }
  return s - tmp;
}

char * my_strcat(char *dest, char *src) {

  int dest_len = my_strlen(dest),
      i = 0;

  if (src == NULL) {
    return NULL;
  }

  while (*src != '\0') {
    dest[dest_len + i] = *src;
    i++;
    src++;
  }
  dest[dest_len + i] = '\0';

  return dest;
}

char * my_strcpy(char *dest, char *src) {
  if (src == NULL) {
    return NULL;
  }
  while (*src != '\0') {
    *dest = *src;
    src++;
    dest++;
  }
  return dest;
}
